module.exports = function (config) {
    config.addPassthroughCopy("src/styles");
    config.addPassthroughCopy("src/assets");
    config.addPassthroughCopy("src/favicon.ico");
    return {
        dir: {
            input: "src",
        }
    };
}