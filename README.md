# My personal site

That's all there is to it. Can be found at [jtroyer.dev](https://jtroyer.dev) 

Also, uses https://marketplace.visualstudio.com/items?itemName=Tobermory.es6-string-html for highlighting, so that's why there's all those `/*html*/` comments everywhere.
