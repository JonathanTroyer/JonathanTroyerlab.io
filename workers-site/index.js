import { getAssetFromKV } from '@cloudflare/kv-asset-handler'

/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false

addEventListener('fetch', event => {
  try {
    event.respondWith(handleEvent(event))
  } catch (e) {
    if (DEBUG) {
      return event.respondWith(
        new Response(e.message || e.toString(), {
          status: 500,
        }),
      )
    }
    event.respondWith(new Response('Internal Error', { status: 500 }))
  }
})

async function handleEvent(event) {
  const url = new URL(event.request.url)
  let options = {}

  try {
    if (DEBUG) {
      // customize caching
      options.cacheControl = {
        bypassCache: true,
      }
    }
    let asset = await getAssetFromKV(event, options);
    if (asset.headers.get('Content-Type').includes('text/html')) {
      let links = await parsePreloads(asset);
      links.forEach(l => {
        asset.headers.append('Link', l);
      })
    }
    return asset;
  } catch (e) {
    // if an error is thrown try to serve the asset at 404.html
    if (!DEBUG) {
      try {
        let notFoundResponse = await getAssetFromKV(event, {
          mapRequestToAsset: req => new Request(`${new URL(req.url).origin}/404.html`, req),
        })

        return new Response(notFoundResponse.body, { ...notFoundResponse, status: 404 })
      } catch (e) { }
    }

    return new Response(e.message || e.toString(), {
      status: 500,
    });
  }
}

/**
 * Parses an html page for preload links and turns them into headers that can be pushed
 *
 * @param {Response} response
 * @returns {Promise<Array<string>>} an array of header values to be appended to the Link header
 */
async function parsePreloads(response) {
  response = response.clone();
  const body = await response.text();
  let links = [];
  body
    .match(/<link.*?rel="pre(load|fetch)".*?>/g)
    .forEach(l => {
      let href = l.match(/href="(.*?)"/)[1];
      let as = l.match(/as="(.*?)"/)[1];
      let rel = l.match(/rel="(.*?)"/)[1];
      let link = `<${href}>; rel=${rel}; as=${as};`
      if (as === 'font')
        link += ' crossorigin;';
      links.push(link);
    });
  return links;
}